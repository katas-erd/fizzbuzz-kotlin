
fun `fizzbuzz of`(n: Int) =  evaluate {
    (n `is multiple of` 3 then { display("fizz") }) or
    (n `is multiple of` 5 then { display("buzz") }) `else`
    { display(n) }
}







private fun evaluate(f: Accumulator.() -> Unit): String {
    val acc = Accumulator()
    acc.f()
    return acc.value
}



private infix fun Int.`is multiple of`(m: Int) = this != 0  && this % m == 0

private infix fun Boolean.then(lazyRight: () -> Boolean): Boolean {
    return this && lazyRight()
}

private infix fun Boolean.`else`(lazyRight: () -> Boolean): Boolean {
    return this || lazyRight()
}

class Accumulator {
    private var result: String = ""

    fun display(s: String): Boolean {
        result += s
        return true
    }

    fun display(s: Int): Boolean {
        result += s.toString()
        return true
    }

    val value: String
        get() = result
}