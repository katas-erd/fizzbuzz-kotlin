import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Order
import org.junit.jupiter.params.ParameterizedTest
import org.junit.jupiter.params.provider.Arguments
import org.junit.jupiter.params.provider.MethodSource
import java.util.stream.Stream

class FizzbuzzTest {
    @ParameterizedTest
    @MethodSource("multipleOf3")
    @Order(1)
    fun `when n is multiple of 3 then display fizz`(n: Int) {
        assertThat(`fizzbuzz of`(n)).startsWith("fizz")
    }

    @ParameterizedTest
    @MethodSource("multipleOf5")
    @Order(2)
    fun `when n is multiple of 5 then display buzz`(n: Int) {
        assertThat(`fizzbuzz of`(n)).endsWith("buzz")
    }

    @ParameterizedTest
    @MethodSource("multipleOf3And5")
    @Order(3)
    fun `when n is multiple of 3 and 5 then display fizzbuzz`(n: Int) {
        assertThat(`fizzbuzz of`(n)).endsWith("fizzbuzz")
    }

    @ParameterizedTest
    @MethodSource("notMultipleOf3Or5")
    @Order(4)
    fun `else display the number`(n: Int) {
        assertThat(`fizzbuzz of`(n)).isEqualTo("$n")
    }

    companion object {
        @JvmStatic
        fun multipleOf3(): Stream<Arguments> {
            return (0..100).step(3).map {
                Arguments.of(it)
            }.drop(1).stream()
        }

        @JvmStatic
        fun multipleOf5(): Stream<Arguments> {
            return (0..100).step(5).map {
                Arguments.of(it)
            }.drop(1).stream()
        }

        @JvmStatic
        fun notMultipleOf3Or5(): Stream<Arguments> {
            val multiples = (multipleOf3().toList() + multipleOf5().toList()).map { it.get().first() as Int }
            return (0..100).filter { !multiples.contains(it) }.map {
                Arguments.of(it)
            }.stream()
        }

        @JvmStatic
        fun multipleOf3And5(): Stream<Arguments> {
            return (0..100).step(15).map {
                Arguments.of(it)
            }.drop(1).stream()
        }
    }
}

